 #define WOKWI

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef WOKWI
#include "pico/stdlib.h"
#endif

#include "pico/double.h" // Required for using double-precision variables.
#include "pico/float.h"  // Required for using single-precision variables.

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
 float singlePi(float a){
    int n = 100000;
    float result=1;
    //loop of 100,000 to compute approx value for pi
    for(int i =0; i<n; i++){
        float tmp = ((a*(i+1))/(1+(i*2)))*((a*(i+1))/(3+(i*2)));
        result = result * tmp;
    }    
    return result;
}
double doublePi(double a){
    int n =100000;
    double result =1;
        //loop of 100,000 to compute approx value for pi
    for(int i=0; i<n; i++){
        double tmp = ((a*(i+1))/(1+(i*2)))*((a*(i+1))/(3+(i*2)));
        result = result * tmp;
    }
    return result;
}
int main() {



    // Print a console message to inform user what's going on.
    float start = 2;
    float result = singlePi(start)*2;
    float pi = 3.14159265359;
    float approxError = result - pi;
    printf("approximate value for single percision PI is %f\n",result);
    printf("approximate error for single percision PI is %f\n",approxError);
    double start1=2;
    double result2 = doublePi(start1)*2;
    float approxError2 = result2 - pi;
    printf("approximate value for double percision PI is %f\n",result2);
    printf("approximate error for double percision PI is %f\n",approxError2);
    // Returning zero indicates everything went okay.
    return 0;
}